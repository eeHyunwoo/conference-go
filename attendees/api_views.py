from django.http import JsonResponse
from common.json import ModelEncoder
from events.models import Conference, Location
from django.views.decorators.http import require_http_methods
import json

from .models import Attendee

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name"
    ]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee,
    properties = [
        "email",
        "name",
        "company_name",
        "created",
    ]
    def get_extra_date(self, o):
        return {
            "conference": o.conference.name}

@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.all()
        return JsonResponse(
            {"attendees": attendees},
            encoder = AttendeeListEncoder,
        )
    else:
        content = json.loads(request.body)
        try: #In case user types in a bad state abbreviation
            conference = Conference.objects.get(id=conference_id) #Since state abbrev in State model, gotta create state property accessing and saving that state value to our dictionary.
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        attendee = Attendee.objects.create(**content) #making the POST request
        return JsonResponse(
            attendee,
            encoder = AttendeeDetailEncoder,
            safe = False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            {"attendee": attendee },
            encoder = AttendeeDetailEncoder,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
